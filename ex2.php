<?php
// khong dung de quy
function fibonacci($n) 
{
    $f0 = 0;
    $f1 = 1;
    $fn = 1;
 
    if ($n < 0) {
        return - 1;
    } elseif ($n == 0 || $n == 1) {
        return $n;
    } else {
        for($i = 2; $i < $n; $i ++) {
            $f0 = $f1;
            $f1 = $fn;
            $fn = $f0 + $f1;
        }
    }
    return $fn;
}

// su dung de quy 
function fibonacci2($n)
{
    if ($n < 0) {
        return - 1;
    } elseif ($n == 0 || $n == 1) {
        return $n;
    } else {
        return fibonacci($n - 1) + fibonacci($n - 2);
    }
}

$output1 = fibonacci(11);
$output2 = fibonacci(11);
echo "$output1\n";
echo "$output2\n";
?>
