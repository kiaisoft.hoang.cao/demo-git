<?php
$string = "aaaabbacccdeeeaaaaaaaaaa";

$count = 1;
$last_char = $string[0];
$output = "";

for ($i = 1; $i < strlen($string); $i++) {
  if ($string[$i] == $last_char) {
    $count++;
  } else {
    $output .= $last_char . $count;
    $count = 1;
    $last_char = $string[$i];
  }
}

$output .= $last_char . $count;

echo "Input: $string\n";
echo "Output: $output\n";
?>
