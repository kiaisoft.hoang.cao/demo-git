<?php
function sortColors(&$nums) {
    $count = count($nums);
    $red = 0;
    $white = 0;
    $blue = $count - 1;
    
    while ($white <= $blue) {
        if ($nums[$white] == 1) {
            $temp = $nums[$white];
            $nums[$white] = $nums[$red];
            $nums[$red] = $temp;
            $red++;
            $white++;
        } elseif ($nums[$white] == 5) {
            $white++;
        } else {
            $temp = $nums[$white];
            $nums[$white] = $nums[$blue];
            $nums[$blue] = $temp;
            $blue--;
        }
    }
}

// Example usage
$nums = [2, 1, 2, 5, 5, 1,1,5,2,1,5,2];
sortColors($nums);
print_r($nums);
?>
