<?php
function returnSum($nums, $target){
    $output = [];
    for($i = 0; $i < count($nums); $i++){
        $val = $target - $nums[$i];
        $index = array_search($val, $nums);
        if($index > $i) {
            $output[] = [$nums[$i],$val];
        }
    }
    return $output;
}

$nums = [2,7,4,5,11,23,8];
$target = 9;
print_r(returnSum($nums, $target));
?>
