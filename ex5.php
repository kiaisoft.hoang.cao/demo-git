<?php
function rotateArray(&$nums, $k) {
    $k = $k % count($nums); // Lấy số lượng phần tử cần xoay
    for ($i = 0; $i < $k; $i++) {
        $temp = array_pop($nums); 
        array_unshift($nums, $temp); 
    }
    return $nums;
}

$nums = [1,2,3,4,5,6,7];
$k =3;
rotateArray($nums,$k);
print_r($nums);
?>
